namespace cyberpet.Models
{
    public class UserTask
    {
        public string UserId { get; set; }
        public int TaskId { get; set; }
        
        public virtual ApplicationUser User { get; set; }
        public virtual Task Task { get; set; }
    }
}