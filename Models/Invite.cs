using System;
using System.ComponentModel.DataAnnotations;

namespace cyberpet.Models
{
    public class Invite
    {
        [Key]
        public Guid Id { get; set; }
        
        public Household Household { get; set; }
        
        public ApplicationUser Inviter { get; set; }
        public string InviteeEmail { get; set; }
        public int HouseholdId { get; set; }
        
    }
}