using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using cyberpet.Models;

public class Task 
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public string Notes { get; set; }
    public bool Completed { get; set; }
    public DateTime? StartTime { get; set; }
    public DateTime? EndTime { get; set; }
    public DateTime? CompletedAt { get; set; }
    public bool Enabled { get; set; }
    public virtual Task OriginTask { get; set; }
    public int? AssignedPetId { get; set; }
    public virtual Household Household { get; set; }
    public virtual Pet AssignedPet { get; set; }
    public virtual List<UserTask> UserTasks { get; set; }
}