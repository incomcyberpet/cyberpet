// https://entityframework.net/knowledge-base/15153390/many-to-many-mapping-with-extra-fields-in-fluent-api
// https://entityframework.net/knowledge-base/7050404/create-code-first--many-to-many--with-additional-fields-in-association-table

using cyberpet.Models;

public class HouseholdUser
{
    public string UserId { get; set; }
    public int HouseholdId { get; set; }
        
    public virtual ApplicationUser User { get; set; }
    public virtual Household Household { get; set; }
    public bool IsAdmin { get; set; }
}