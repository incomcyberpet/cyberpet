using System.Collections.Generic;

namespace cyberpet.Models
{
    public class HouseholdViewModel
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public int Id { get; set; }
        public string OwnerId { get; set; }

        public List<Pet> Pets { get; set; }
        
        public HouseholdViewModel(Household h)
        {
            Address = h.Address;
            Name = h.Name;
            Id = h.Id;
            OwnerId = h.Owner?.Id;
            Pets = h.Pets;
        }
    }
}