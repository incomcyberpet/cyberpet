using System.ComponentModel.DataAnnotations;

namespace cyberpet.Models
{
    public class RegisterModel  
    {  
        [EmailAddress]
        [Required(ErrorMessage = "User Name is required")]  
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is required")]  
        public string Password { get; set; }

        [Required(ErrorMessage = "Display Name is required")]  
        public string DisplayName { get; set; }
    }  
}