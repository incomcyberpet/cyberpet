using System.ComponentModel.DataAnnotations;

namespace cyberpet.Models
{
    public class InviteRequestModel
    {
        [EmailAddress]
        [Required(ErrorMessage = "Email is Required")]
        public string Email { get; set; }
    }
}