using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace cyberpet.Models 
{
    public class Pet 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Breed { get; set; }
        public int Age { get; set; }
        public double Height { get; set; }
        public double Weight { get; set; }
        public string Species { get; set; }
        public virtual Household Household { get; set; }
        public virtual DateTime LastVetVisit { get; set; }
    }
}