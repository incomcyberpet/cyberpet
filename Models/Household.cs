using System.Collections.Generic;

namespace cyberpet.Models
{
    public class Household
    {
        public virtual List<HouseholdUser> HouseholdUsers { get; set; }
        public virtual ApplicationUser Owner { get; set; }
        public virtual List<Pet> Pets { get; set; } = new List<Pet>();
        public int Id { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }
    }
}