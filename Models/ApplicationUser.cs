using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;

namespace cyberpet.Models
{
    // Add profile data for application users by adding properties to the AppUser class
    public class ApplicationUser : IdentityUser
    {
        public string ProfilePictureUrl { get; set; }
        public string DisplayName { get; set; }
        
        public virtual List<Household> OwnedHouseholds { get; set; }
        [JsonIgnore]
        public virtual List<HouseholdUser> HouseholdUsers { get; set; } 
        [JsonIgnore]
        public virtual List<UserTask> UserTasks { get; set; }
        [JsonIgnore]
        public override string PasswordHash { get; set; }
        public override string SecurityStamp { get; set; }
    }
}
