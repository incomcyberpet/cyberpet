﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace cyberpet.Migrations
{
    public partial class ProperNamingConvention : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationUserTask_AspNetUsers_assignedUsersId",
                table: "ApplicationUserTask");

            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationUserTask_Tasks_AssignedTasksid",
                table: "ApplicationUserTask");

            migrationBuilder.DropForeignKey(
                name: "FK_Pets_Households_householdid",
                table: "Pets");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Households_householdid",
                table: "Tasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Pets_assignedPetid",
                table: "Tasks");

            migrationBuilder.RenameColumn(
                name: "start",
                table: "Tasks",
                newName: "Start");

            migrationBuilder.RenameColumn(
                name: "originTask",
                table: "Tasks",
                newName: "OriginTask");

            migrationBuilder.RenameColumn(
                name: "notes",
                table: "Tasks",
                newName: "Notes");

            migrationBuilder.RenameColumn(
                name: "name",
                table: "Tasks",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "householdid",
                table: "Tasks",
                newName: "HouseholdId");

            migrationBuilder.RenameColumn(
                name: "end",
                table: "Tasks",
                newName: "End");

            migrationBuilder.RenameColumn(
                name: "enabled",
                table: "Tasks",
                newName: "Enabled");

            migrationBuilder.RenameColumn(
                name: "done",
                table: "Tasks",
                newName: "Done");

            migrationBuilder.RenameColumn(
                name: "description",
                table: "Tasks",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "completed",
                table: "Tasks",
                newName: "Completed");

            migrationBuilder.RenameColumn(
                name: "assignedPetid",
                table: "Tasks",
                newName: "AssignedPetId");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "Tasks",
                newName: "Id");

            migrationBuilder.RenameIndex(
                name: "IX_Tasks_householdid",
                table: "Tasks",
                newName: "IX_Tasks_HouseholdId");

            migrationBuilder.RenameIndex(
                name: "IX_Tasks_assignedPetid",
                table: "Tasks",
                newName: "IX_Tasks_AssignedPetId");

            migrationBuilder.RenameColumn(
                name: "weight",
                table: "Pets",
                newName: "Weight");

            migrationBuilder.RenameColumn(
                name: "species",
                table: "Pets",
                newName: "Species");

            migrationBuilder.RenameColumn(
                name: "name",
                table: "Pets",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "lastVetVisit",
                table: "Pets",
                newName: "LastVetVisit");

            migrationBuilder.RenameColumn(
                name: "householdid",
                table: "Pets",
                newName: "HouseholdId");

            migrationBuilder.RenameColumn(
                name: "height",
                table: "Pets",
                newName: "Height");

            migrationBuilder.RenameColumn(
                name: "breed",
                table: "Pets",
                newName: "Breed");

            migrationBuilder.RenameColumn(
                name: "age",
                table: "Pets",
                newName: "Age");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "Pets",
                newName: "Id");

            migrationBuilder.RenameIndex(
                name: "IX_Pets_householdid",
                table: "Pets",
                newName: "IX_Pets_HouseholdId");

            migrationBuilder.RenameColumn(
                name: "isAdmin",
                table: "HouseholdUsers",
                newName: "IsAdmin");

            migrationBuilder.RenameColumn(
                name: "name",
                table: "Households",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "address",
                table: "Households",
                newName: "Address");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "Households",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "ProfilePictureURL",
                table: "AspNetUsers",
                newName: "ProfilePictureUrl");

            migrationBuilder.RenameColumn(
                name: "assignedUsersId",
                table: "ApplicationUserTask",
                newName: "AssignedUsersId");

            migrationBuilder.RenameColumn(
                name: "AssignedTasksid",
                table: "ApplicationUserTask",
                newName: "AssignedTasksId");

            migrationBuilder.RenameIndex(
                name: "IX_ApplicationUserTask_assignedUsersId",
                table: "ApplicationUserTask",
                newName: "IX_ApplicationUserTask_AssignedUsersId");

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationUserTask_AspNetUsers_AssignedUsersId",
                table: "ApplicationUserTask",
                column: "AssignedUsersId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationUserTask_Tasks_AssignedTasksId",
                table: "ApplicationUserTask",
                column: "AssignedTasksId",
                principalTable: "Tasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Pets_Households_HouseholdId",
                table: "Pets",
                column: "HouseholdId",
                principalTable: "Households",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Households_HouseholdId",
                table: "Tasks",
                column: "HouseholdId",
                principalTable: "Households",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Pets_AssignedPetId",
                table: "Tasks",
                column: "AssignedPetId",
                principalTable: "Pets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationUserTask_AspNetUsers_AssignedUsersId",
                table: "ApplicationUserTask");

            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationUserTask_Tasks_AssignedTasksId",
                table: "ApplicationUserTask");

            migrationBuilder.DropForeignKey(
                name: "FK_Pets_Households_HouseholdId",
                table: "Pets");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Households_HouseholdId",
                table: "Tasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Pets_AssignedPetId",
                table: "Tasks");

            migrationBuilder.RenameColumn(
                name: "Start",
                table: "Tasks",
                newName: "start");

            migrationBuilder.RenameColumn(
                name: "OriginTask",
                table: "Tasks",
                newName: "originTask");

            migrationBuilder.RenameColumn(
                name: "Notes",
                table: "Tasks",
                newName: "notes");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Tasks",
                newName: "name");

            migrationBuilder.RenameColumn(
                name: "HouseholdId",
                table: "Tasks",
                newName: "householdid");

            migrationBuilder.RenameColumn(
                name: "End",
                table: "Tasks",
                newName: "end");

            migrationBuilder.RenameColumn(
                name: "Enabled",
                table: "Tasks",
                newName: "enabled");

            migrationBuilder.RenameColumn(
                name: "Done",
                table: "Tasks",
                newName: "done");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "Tasks",
                newName: "description");

            migrationBuilder.RenameColumn(
                name: "Completed",
                table: "Tasks",
                newName: "completed");

            migrationBuilder.RenameColumn(
                name: "AssignedPetId",
                table: "Tasks",
                newName: "assignedPetid");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Tasks",
                newName: "id");

            migrationBuilder.RenameIndex(
                name: "IX_Tasks_HouseholdId",
                table: "Tasks",
                newName: "IX_Tasks_householdid");

            migrationBuilder.RenameIndex(
                name: "IX_Tasks_AssignedPetId",
                table: "Tasks",
                newName: "IX_Tasks_assignedPetid");

            migrationBuilder.RenameColumn(
                name: "Weight",
                table: "Pets",
                newName: "weight");

            migrationBuilder.RenameColumn(
                name: "Species",
                table: "Pets",
                newName: "species");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Pets",
                newName: "name");

            migrationBuilder.RenameColumn(
                name: "LastVetVisit",
                table: "Pets",
                newName: "lastVetVisit");

            migrationBuilder.RenameColumn(
                name: "HouseholdId",
                table: "Pets",
                newName: "householdid");

            migrationBuilder.RenameColumn(
                name: "Height",
                table: "Pets",
                newName: "height");

            migrationBuilder.RenameColumn(
                name: "Breed",
                table: "Pets",
                newName: "breed");

            migrationBuilder.RenameColumn(
                name: "Age",
                table: "Pets",
                newName: "age");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Pets",
                newName: "id");

            migrationBuilder.RenameIndex(
                name: "IX_Pets_HouseholdId",
                table: "Pets",
                newName: "IX_Pets_householdid");

            migrationBuilder.RenameColumn(
                name: "IsAdmin",
                table: "HouseholdUsers",
                newName: "isAdmin");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Households",
                newName: "name");

            migrationBuilder.RenameColumn(
                name: "Address",
                table: "Households",
                newName: "address");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Households",
                newName: "id");

            migrationBuilder.RenameColumn(
                name: "ProfilePictureUrl",
                table: "AspNetUsers",
                newName: "ProfilePictureURL");

            migrationBuilder.RenameColumn(
                name: "AssignedUsersId",
                table: "ApplicationUserTask",
                newName: "assignedUsersId");

            migrationBuilder.RenameColumn(
                name: "AssignedTasksId",
                table: "ApplicationUserTask",
                newName: "AssignedTasksid");

            migrationBuilder.RenameIndex(
                name: "IX_ApplicationUserTask_AssignedUsersId",
                table: "ApplicationUserTask",
                newName: "IX_ApplicationUserTask_assignedUsersId");

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationUserTask_AspNetUsers_assignedUsersId",
                table: "ApplicationUserTask",
                column: "assignedUsersId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationUserTask_Tasks_AssignedTasksid",
                table: "ApplicationUserTask",
                column: "AssignedTasksid",
                principalTable: "Tasks",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Pets_Households_householdid",
                table: "Pets",
                column: "householdid",
                principalTable: "Households",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Households_householdid",
                table: "Tasks",
                column: "householdid",
                principalTable: "Households",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Pets_assignedPetid",
                table: "Tasks",
                column: "assignedPetid",
                principalTable: "Pets",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
