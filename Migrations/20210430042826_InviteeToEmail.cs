﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace cyberpet.Migrations
{
    public partial class InviteeToEmail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Invites_AspNetUsers_InviteeId1",
                table: "Invites");

            migrationBuilder.DropIndex(
                name: "IX_Invites_InviteeId1",
                table: "Invites");

            migrationBuilder.DropColumn(
                name: "InviteeId",
                table: "Invites");

            migrationBuilder.DropColumn(
                name: "InviteeId1",
                table: "Invites");

            migrationBuilder.AddColumn<string>(
                name: "InviteeEmail",
                table: "Invites",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InviteeEmail",
                table: "Invites");

            migrationBuilder.AddColumn<int>(
                name: "InviteeId",
                table: "Invites",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "InviteeId1",
                table: "Invites",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Invites_InviteeId1",
                table: "Invites",
                column: "InviteeId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Invites_AspNetUsers_InviteeId1",
                table: "Invites",
                column: "InviteeId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
