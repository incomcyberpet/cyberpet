﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace cyberpet.Migrations
{
    public partial class TasksUsers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Tasks_Taskid",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_Taskid",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Taskid",
                table: "AspNetUsers");

            migrationBuilder.CreateTable(
                name: "ApplicationUserTask",
                columns: table => new
                {
                    AssignedTasksid = table.Column<int>(type: "int", nullable: false),
                    assignedUsersId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationUserTask", x => new { x.AssignedTasksid, x.assignedUsersId });
                    table.ForeignKey(
                        name: "FK_ApplicationUserTask_AspNetUsers_assignedUsersId",
                        column: x => x.assignedUsersId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ApplicationUserTask_Tasks_AssignedTasksid",
                        column: x => x.AssignedTasksid,
                        principalTable: "Tasks",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationUserTask_assignedUsersId",
                table: "ApplicationUserTask",
                column: "assignedUsersId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApplicationUserTask");

            migrationBuilder.AddColumn<int>(
                name: "Taskid",
                table: "AspNetUsers",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_Taskid",
                table: "AspNetUsers",
                column: "Taskid");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Tasks_Taskid",
                table: "AspNetUsers",
                column: "Taskid",
                principalTable: "Tasks",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
