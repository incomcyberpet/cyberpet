﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace cyberpet.Migrations
{
    public partial class TaskDoneToCompleted : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Done",
                table: "Tasks",
                newName: "Completed");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Completed",
                table: "Tasks",
                newName: "Done");
        }
    }
}
