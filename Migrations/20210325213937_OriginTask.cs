﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace cyberpet.Migrations
{
    public partial class OriginTask : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OriginTask",
                table: "Tasks");

            migrationBuilder.AddColumn<int>(
                name: "OriginTaskId",
                table: "Tasks",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_OriginTaskId",
                table: "Tasks",
                column: "OriginTaskId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Tasks_OriginTaskId",
                table: "Tasks",
                column: "OriginTaskId",
                principalTable: "Tasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Tasks_OriginTaskId",
                table: "Tasks");

            migrationBuilder.DropIndex(
                name: "IX_Tasks_OriginTaskId",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "OriginTaskId",
                table: "Tasks");

            migrationBuilder.AddColumn<int>(
                name: "OriginTask",
                table: "Tasks",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
