﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace cyberpet.Migrations
{
    public partial class HouseholdOwner : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "OwnerId",
                table: "Households",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Households_OwnerId",
                table: "Households",
                column: "OwnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Households_AspNetUsers_OwnerId",
                table: "Households",
                column: "OwnerId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Households_AspNetUsers_OwnerId",
                table: "Households");

            migrationBuilder.DropIndex(
                name: "IX_Households_OwnerId",
                table: "Households");

            migrationBuilder.DropColumn(
                name: "OwnerId",
                table: "Households");
        }
    }
}
