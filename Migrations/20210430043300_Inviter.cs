﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace cyberpet.Migrations
{
    public partial class Inviter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "InviterId",
                table: "Invites",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Invites_InviterId",
                table: "Invites",
                column: "InviterId");

            migrationBuilder.AddForeignKey(
                name: "FK_Invites_AspNetUsers_InviterId",
                table: "Invites",
                column: "InviterId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Invites_AspNetUsers_InviterId",
                table: "Invites");

            migrationBuilder.DropIndex(
                name: "IX_Invites_InviterId",
                table: "Invites");

            migrationBuilder.DropColumn(
                name: "InviterId",
                table: "Invites");
        }
    }
}
