# CyberPet Backend

To run this app, you'll need MS SQL Server - download the developer version if on Windows, or follow instructions [here](https://www.twilio.com/blog/using-sql-server-on-macos-with-docker) for setting it up through Docker on Mac.

`Controllers/RegistrationController.cs` defines the `/register` endpoint, which runs the logic of creating a user account. 

The mobile app will then request a token from `IdentityServer` (docs [here](https://docs.identityserver.io/en/latest/index.html)), and will be able to authenticate to future requests.


## DB Configuration
Use the same username and password from the tutorial to set up your SQL Server on Docker.
```
 username: sa
 password: someThingComplicated1234;
```

The resulting connection string which you should add to your user secrets under `Db:ConnectionString` will be
```
Data Source=localhost;Initial Catalog=cyberpet;User Id=sa; Password=someThingComplicated1234;
```

This is the command to set the connection string as a secret so the app can load it:  

```bash
dotnet user-secrets set "Db:ConnectionString" "Data Source=localhost;Initial Catalog=cyberpet;User Id=sa; Password=someThingComplicated1234;"
```

The code that sets this up: 
```c#
// Program.cs
Host.CreateDefaultBuilder(args)
    .ConfigureAppConfiguration((hostContext, builder) =>
    {
        if (hostContext.HostingEnvironment.IsDevelopment())
        {
            builder.AddUserSecrets<Program>();
        }
    })
    
// Startup.cs
private string _dbConnectionString = null;
...
_dbConnectionString = Configuration["Db:ConnectionString"];

```

