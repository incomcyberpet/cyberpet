using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using cyberpet.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace cyberpet.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiController]
    [Route("api/[controller]")]
    public class HouseholdsController : Controller
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<ApplicationUser> _userManager;
        
        public HouseholdsController(ApplicationDbContext context, UserManager<ApplicationUser> users)
        {
            _dbContext = context;
            _userManager = users;
        }

        [HttpPost]
        public async Task<IActionResult> CreateHousehold([FromBody] Household household)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var user = await _userManager.FindByIdAsync(userId);
            
            household.Owner = user;
            
            await _dbContext.Households.AddAsync(household);
            await _dbContext.HouseholdUsers.AddAsync(new HouseholdUser()
            {
                Household = household,
                IsAdmin = true,
                User = user,
            });
            await _dbContext.SaveChangesAsync();

            return Ok(new HouseholdViewModel(household));
        }
        
        [HttpGet]
        public IEnumerable<HouseholdViewModel> GetCurrentUserHouseholds()
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            return _dbContext.HouseholdUsers
                .Where(hu => hu.UserId == userId)
                .Include(hu => hu.Household.Owner)
                .Select(hu => new HouseholdViewModel(hu.Household))
                .ToList();
        }

        [HttpGet("{householdId:int}")]
        public IActionResult GetOneHousehold(int householdId)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            try
            {
                var userHouseholds = _dbContext.HouseholdUsers
                    .Where(h => h.HouseholdId == householdId && h.UserId == userId)
                    .Include(t => t.Household.Owner)
                    .First();

                return Ok(new HouseholdViewModel(userHouseholds.Household));
            }
            catch (InvalidOperationException e)
            {
                return NotFound();
            }
        }

        [HttpDelete("{householdId:int}")]
        public async Task<IActionResult> DeleteHousehold(int householdId)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            
            try
            {
                Household householdToDelete = null;
                householdToDelete = await _dbContext.Households
                    .Where(h => h.Id == householdId && h.Owner.Id == userId)
                    .FirstOrDefaultAsync<Household>();
                
                if (householdToDelete == null)
                {
                    return NotFound();
                }
                
                var petsToDelete = _dbContext.Pets
                    .Where(p => p.Household.Id == householdId);
                var tasksToDelete = _dbContext.Tasks
                    .Where(t => t.Household.Id == householdId);

                _dbContext.Pets.RemoveRange(petsToDelete);
                _dbContext.Tasks.RemoveRange(tasksToDelete);
                _dbContext.Households.Remove(householdToDelete);
                
                await _dbContext.SaveChangesAsync();
                
                return Ok(householdToDelete);
            }
            catch (InvalidOperationException e)
            {
                return NotFound();
            }
        }

        [HttpPatch("{householdId:int}")]
        public async Task<IActionResult> ModifyHousehold(int householdId, [FromBody] Household household)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            try
            {
                Household householdToModify = null;
                householdToModify = await _dbContext.Households
                    .Where(h => h.Id == householdId && h.Owner.Id == userId)
                    .FirstOrDefaultAsync<Household>();

                if (householdToModify == null)
                {
                    return NotFound();
                }

                householdToModify.Name = household.Name;
                householdToModify.Address = household.Address;

                await _dbContext.SaveChangesAsync();

                return Ok(householdToModify);
            }
            catch (InvalidOperationException e)
            {
                return NotFound();
            }
        }

        [HttpPost]
        [Route("{householdId:int}/invite")]
        public async Task<IActionResult> Invite(int householdId, [FromBody] InviteRequestModel invitation)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var user = await _userManager.FindByIdAsync(userId);
    
            var householdUser = await _dbContext.HouseholdUsers.FirstOrDefaultAsync(hu => hu.HouseholdId == householdId 
                                                                    && hu.UserId == userId
                                                                    && hu.IsAdmin);

            if (householdUser == null)
            {
                return NotFound();
            }

            Models.Invite invite = new Invite
            {
                Household = householdUser.Household,
                InviteeEmail = invitation.Email,
                HouseholdId = householdUser.HouseholdId,
                Inviter = user
            };

            await _dbContext.Invites.AddAsync(invite);
            await _dbContext.SaveChangesAsync();
            
            return Ok(invite);
        }
    }
}