using System;
using System.Threading.Tasks;
using cyberpet.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RegisterModel = cyberpet.Models.RegisterModel;

namespace cyberpet.Controllers

{  
    [Route("api/auth")]  
    [ApiController]  
    public class RegistrationController : ControllerBase  
    {  
        private readonly UserManager<ApplicationUser> userManager;  
        private readonly RoleManager<IdentityRole> roleManager;  
        private readonly SignInManager<ApplicationUser> signInManager;  
  
        public RegistrationController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, SignInManager<ApplicationUser> signInManager)  
        {  
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.signInManager = signInManager;
        }  
  
        [HttpPost]  
        [Route("register")]  
        public async Task<IActionResult> Register([FromBody] RegisterModel model)  
        {  
            var userExists = await userManager.FindByNameAsync(model.Email);  
            if (userExists != null)  
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "User already exists!" });  
  
            ApplicationUser user = new ApplicationUser()  
            {  
                Email = model.Email,  
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.Email,
                DisplayName = model.DisplayName
            };  
            
            var result = await userManager.CreateAsync(user, model.Password);  
            if (!result.Succeeded)  
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "User creation failed! Please check user details and try again." });

            await signInManager.SignInAsync(user, false);
            return Ok(new Response { Status = "Success", Message = "User created successfully!" });  
        }
    }  
}  