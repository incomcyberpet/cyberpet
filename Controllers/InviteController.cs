using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using cyberpet.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace cyberpet.Controllers
{
    [ApiController]
    public class InviteController : Controller
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<ApplicationUser> userManager;  

        public InviteController(ApplicationDbContext dbContext, UserManager<ApplicationUser> um)
        {
            _dbContext = dbContext;
            userManager = um;
        }
        
        [HttpGet]
        [Route("/acceptInvite/{inviteId}")]
        public async Task<IActionResult> Index(Guid inviteId)
        {
            var invite = await _dbContext.Invites.Include(i => i.Household)
                .Include(i => i.Inviter).FirstOrDefaultAsync(i => i.Id == inviteId);

            if (invite == null)
            {
                return NotFound();
            }

            var invitee = await userManager.FindByEmailAsync(invite.InviteeEmail);

            if (invitee == null)
            {
                // need to make new user
                ViewData["newUser"] = true;
            }
            else
            {
                ViewData["newUser"] = false;
            }
            
            ViewData["invite"] = invite;

            return View();
        }

        [HttpPost]
        [Route("acceptInvite/{inviteId}")]
        public async Task<IActionResult> CreateUser(Guid inviteId, [FromForm] RegisterModel model)
        {
            var userExists = await userManager.FindByNameAsync(model.Email);  
            if (userExists != null)  
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "User already exists!" });  
  
            Debug.WriteLine("user exists");
            ApplicationUser user = new ApplicationUser()  
            {  
                Email = model.Email,  
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.Email,
                DisplayName = model.DisplayName
            };

            ViewBag.inviteId = inviteId;
            
            var result = await userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                ViewBag.essage = "User creation failed! Please check user details and try again.";
            }
            else
            {
                Console.WriteLine("user created");
                ViewBag.message = "User Created Successfully!";
            }
            
            return View();
        }

        [HttpPatch]
        [Route("acceptInvite/{inviteId}")]
        public async Task<IActionResult> AddToHousehold(Guid inviteId)
        {
            var invite = await _dbContext.Invites.FirstOrDefaultAsync(i => i.Id == inviteId);
            if (invite == null)
                return NotFound();
            
            var user = await userManager.FindByNameAsync(invite.InviteeEmail);
            if (user == null)
                return NotFound();

            var hu = new HouseholdUser
            {
                Household = invite.Household,
                HouseholdId = invite.HouseholdId,
                IsAdmin = false,
                User = user
            };
            var result = await _dbContext.HouseholdUsers.AddAsync(hu);
            if (result == null)
                return NotFound();

            _dbContext.Invites.Remove(invite);
            await _dbContext.SaveChangesAsync();
            
            Console.WriteLine("Invite accepted");
            return Ok();
        }
    }
}