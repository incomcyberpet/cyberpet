using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using cyberpet.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace cyberpet.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiController]
    [Route("api")]
    public class PetsController : ControllerBase
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<ApplicationUser> _userManager;
        
        public PetsController(ApplicationDbContext context, UserManager<ApplicationUser> users)
        {
            _dbContext = context;
            _userManager = users;
        }
        
        [HttpGet("households/{householdId}/pets")]
        public IActionResult GetHouseholdPets(int householdId)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            
            if (!_dbContext.HouseholdUsers.Any(h => h.HouseholdId == householdId && h.UserId == userId))
                return NotFound();
            
            try
            {
                var pets = _dbContext.Pets.Where(p => p.Household.Id == householdId);
                return Ok(pets);
            }
            catch (InvalidOperationException e)
            {
                return NotFound();
            }
        }

        [HttpGet("households/{householdId}/pets/{petId}")]
        public async Task<IActionResult> GetOnePet(int householdId, int petId)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (!_dbContext.HouseholdUsers.Any(h => h.HouseholdId == householdId && h.UserId == userId))
                return NotFound();
            
            try
            {
                var pet = await _dbContext.Pets
                    .FirstAsync(p => p.Household.Id == householdId && p.Id == petId);

                return Ok(pet);
            }
            catch (InvalidOperationException e)
            {
                return NotFound();
            }
        }
        
        [HttpPost]
        [Route("households/{householdId}/pets")]
        public async Task<IActionResult> CreatePet([FromBody] Pet pet, int householdId)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (!_dbContext.HouseholdUsers.Any(hu => hu.UserId == userId && hu.HouseholdId == householdId))
                return NotFound();

            try
            {
                var house = _dbContext.Households.First(h => h.Id == householdId);

                house.Pets.Add(pet);
                await _dbContext.SaveChangesAsync();
            }
            catch (InvalidOperationException e)
            {
                return NotFound();
            }

            return Ok(pet);
        }
        
        [HttpDelete]
        [Route("households/{householdId}/pets/{petId}")]
        public async Task<IActionResult> DeletePet(int householdId, int petId)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            var pet = await _dbContext.Pets
                .Include(p=> p.Household)
                .Where(p=> p.Id == petId && p.Household.Owner.Id == userId)
                .FirstOrDefaultAsync();
            
            if (pet == null)
            {
                return NotFound();
            }

            if (pet.Household.Id != householdId)
            {
                return BadRequest();
            }

            try
            {
                var tasksToDelete = _dbContext.Tasks.Where(t => t.AssignedPet.Id == petId);
                
                _dbContext.Pets.Remove(pet);
                _dbContext.Tasks.RemoveRange(tasksToDelete);
                
                await _dbContext.SaveChangesAsync();
                
                return Ok(pet);
            }
            catch (ArgumentNullException e)
            {
                return BadRequest();
            }
        }

        [HttpPatch]
        [Route("households/{householdId}/pets/{petId}")]
        public async Task<IActionResult> ModifyPet(int householdId, int petId, [FromBody] Pet pet)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            try
            {

                if (!_dbContext.HouseholdUsers.Any(h => h.UserId == userId && h.HouseholdId == householdId))
                    return NotFound();

                var household = await _dbContext.Households.FirstOrDefaultAsync(h => h.Id == householdId);
                if (household == null)
                    return NotFound();

                var petToModify = await _dbContext.Pets.FirstOrDefaultAsync(p => p.Id == petId && p.Household.Id == householdId);
                if (pet == null)
                    return NotFound();

                petToModify.Age = pet.Age;
                petToModify.Breed = pet.Breed;
                petToModify.Height = pet.Height;
                petToModify.Name = pet.Name;
                petToModify.Species = pet.Species;
                petToModify.Weight = pet.Weight;
                petToModify.LastVetVisit = pet.LastVetVisit;

                await _dbContext.SaveChangesAsync();
                
                return Ok(petToModify);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }
    }
}
