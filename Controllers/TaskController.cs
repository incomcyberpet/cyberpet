using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using cyberpet.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.TagHelpers.Cache;
using Microsoft.AspNetCore.Routing.Template;
using Microsoft.EntityFrameworkCore;

namespace cyberpet.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiController]
    [Route("api")]
    public class TasksController : ControllerBase
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<ApplicationUser> _userManager;

        public TasksController(ApplicationDbContext context, UserManager<ApplicationUser> users)
        {
            _dbContext = context;
            _userManager = users;
        }

        [HttpPost(template: "households/{householdId}/tasks")]
        public async Task<IActionResult> CreateTask(int householdId, [FromBody] Task task)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var user = await _userManager.FindByIdAsync(userId);

            var h = _dbContext.Households.FindAsync(householdId).Result;

            task.Household = h;

            await _dbContext.Tasks.AddAsync(task);
            await _dbContext.SaveChangesAsync();

            return Ok(task);
        }

        [HttpGet("households/{householdId}/tasks")]
        public async Task<IActionResult> GetHouseholdTasks(int householdId)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var user = await _userManager.FindByIdAsync(userId);

            if (!_dbContext.HouseholdUsers.Any(h => h.HouseholdId == householdId && h.UserId == userId))
                return NotFound();

            try
            {
                var task = _dbContext.Tasks
                    .Where(t => t.Household.Id == householdId);
                return Ok(task);
            }
            catch (InvalidOperationException e)
            {
                return NotFound();
            }
        }

        [HttpGet("households/{householdId}/tasks/{taskId}")]
        public async Task<IActionResult> GetOneTask(int householdId, int taskId)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var user = await _userManager.FindByIdAsync(userId);

            if (!_dbContext.HouseholdUsers.Any(hu => hu.HouseholdId == householdId && hu.UserId == userId))
                return NotFound();

            var task = await _dbContext.Tasks
                .Include(t => t.AssignedPet)
                .FirstOrDefaultAsync(t => t.Id == taskId && t.Household.Id == householdId);
            
            if (task == null)
                return NotFound();

            return Ok(task);
        }
        
        [HttpPatch("households/{householdId}/tasks/{taskId}/assign/user/{userId}")]
        public async Task<IActionResult> AssignTaskToUser(int householdId, int taskId, string userId)
        {
            var requestUserId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var requestUser = await _userManager.FindByIdAsync(userId);

            if (!_dbContext.HouseholdUsers.Any(h => h.HouseholdId == householdId && h.UserId == userId))
                return NotFound();

            var task = await _dbContext.Tasks.FirstOrDefaultAsync(t => t.Id == taskId && t.Household.Id == householdId);
            if (task == null)
            {
                return NotFound();
            }

            var userTask = new UserTask()
            {
                Task = task,
                TaskId = taskId,
                UserId = userId,
                User = requestUser
            };

            await _dbContext.UserTasks.AddAsync(userTask);
            await _dbContext.SaveChangesAsync();

            return Ok(task);
        }
        
        [HttpPatch("households/{householdId}/tasks/{taskId}/unassign/user/{userId}")]
        public async Task<IActionResult> UnAssignTaskFromUser(int householdId, int taskId, string userId)
        {
            var requestUserId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var requestUser = await _userManager.FindByIdAsync(userId);

            if (!_dbContext.HouseholdUsers.Any(h => h.HouseholdId == householdId && h.UserId == userId))
                return NotFound();

            var task = await _dbContext.Tasks.FirstOrDefaultAsync(t => t.Id == taskId && t.Household.Id == householdId);
            if (task == null)
            {
                return NotFound();
            }

            var userTask = await _dbContext.UserTasks.FirstOrDefaultAsync(u => u.TaskId == taskId && u.UserId == userId);
            if (userTask == null)
                return Ok();

            _dbContext.UserTasks.Remove(userTask);
            await _dbContext.SaveChangesAsync();

            return Ok(task);
        }


        [HttpDelete(template: "households/{householdId}/tasks/{taskId}")]
        public async Task<IActionResult> DeleteTask(int householdId, int taskId)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var user = await _userManager.FindByIdAsync(userId);

            if (!_dbContext.HouseholdUsers.Any(h => h.HouseholdId == householdId && h.UserId == userId))
                return NotFound();

            var task = await _dbContext.Tasks.FirstOrDefaultAsync(t => t.Id == taskId);

            if (task == null)
            {
                return NotFound();
            }

            if (task.Household.Id != householdId)
            {
                return BadRequest();
            }

            _dbContext.Tasks.Remove(task);
            await _dbContext.SaveChangesAsync();

            return Ok(task);
        }
    }
}