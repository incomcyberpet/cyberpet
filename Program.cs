using cyberpet.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace cyberpet
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostContext, builder) =>
                {
                    if (hostContext.HostingEnvironment.IsDevelopment())
                    {
                        builder.AddUserSecrets<Program>();
                    }
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    logging.AddConsole();
                });
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser> {
        public DbSet<Household> Households { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Pet> Pets { get; set; }
        public DbSet<HouseholdUser> HouseholdUsers { get; set; }
        public DbSet<UserTask> UserTasks { get; set; }
        
        public DbSet<Invite> Invites { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<HouseholdUser>()
                .HasKey(h => new { h.HouseholdId, h.UserId});

            builder.Entity<UserTask>()
                .HasKey(h => new { h.TaskId, h.UserId});

            builder.Entity<Invite>().Property(x => x.Id).HasDefaultValueSql("NEWID()");
            base.OnModelCreating(builder);
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) {
        }
    }
}
